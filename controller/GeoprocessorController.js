const {GeometryType} = require("../enum/GeometryType");
const Store = require("../Store");
const {SpatialRelEnum} = require("../enum/SpatialRelEnum");
const {messages} = require("../messages");
const GeoExecutor = require("../helper/GeoExecutor");
const tokml = require('tokml')
const toGeoJSON = require('@mapbox/togeojson')
const DOMParser = require('xmldom').DOMParser
const shapefile = require('shapefile')
const Zip = require('node-zip')
const shpWrite = require('shp-write')
const moment = require('moment')
const Cache = require('../helper/Cache')
const config = require("../config")
const request = require("request");

class GeoprocessorController {

  constructor(request, response) {
    this.request = request;
    this.response = response;
    this.system = 'GEOPROCESSOR'
  }

  query(params) {
    params.system = params.system || this.system
    return new GeoExecutor(this.request, this.response).exec(params.serviceId || params.id, 'query', params);
  }

  async getCoordinatesFromAddress(params) {
    params.system = params.system || this.system
    if (params.address.length <= 8 && config.webservice){
      try {
        params.address = params.address.replace('.','').replace('-','')
        params.address = await this._getAddressFromCEPANTT(params.address, params.system)
      }catch (e) {

      }
    }
    let cache = await Cache.getCache('getCoordinatesFromAddress', params)
    if (cache) {
      if (this.response) {
        this.response.setHeader('content-type', 'application/json');
      }
      return cache
    }
    return new GeoExecutor(this.request, this.response).exec(params.service, 'getCoordinatesFromAddress', params);
  }

  _prepareParams(params) {
    params.system = params.system || this.system
    let serviceMetadata = Store.get('service:municipios')
    params.returnGeometry = false;
    if (params.type.toUpperCase() == 'UF') {
      if (!params.area) {
        throw new Error('O parâmetro \'area\' é obrigatório para tipo \'UF\'');
      }
      params.where = serviceMetadata.nomeCampoUf + " = '" + params.area.toUpperCase() + "'";
    } else if (params.type.toUpperCase() == 'MUN') {
      params.where = serviceMetadata.nomeCampoGeoCodigo + " = '" + params.area + "' or " + serviceMetadata.nomeCampoMunicipio + " = '" + params.area.toUpperCase() + "'";
    } else {
      params.where = '1=1';
    }
    params.inSR = 4326;
    params.outFields = ['*'];
    params.geometry = {};
    params.geometry.lng = params.lon;
    params.geometry.lat = params.lat;
    params.spatialRel = SpatialRelEnum.Intersects;
    params.geometryType = GeometryType.POINT;
    return params;
  }

  intersect(params) {
    params.system = params.system || this.system
    return new Promise((resolve, reject) => {
      params = this._prepareParams(params);
      let geo = new GeoExecutor(this.request, this.response);
      geo.exec('municipios', 'query', params)
        .then(res => {
          if (!res.features || res.features.length == 0) {
            resolve(false)
          } else {
            resolve(true)
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  getPointInfo(params) {
    params.system = params.system || this.system
    return new Promise((resolve, reject) => {
      params.where = '1=1';
      params.inSR = 4326;
      params.outFields = ['*'];
      params.geometry = {};
      params.geometry.lng = params.lon;
      params.geometry.lat = params.lat;
      params.spatialRel = SpatialRelEnum.Intersects;
      params.geometryType = GeometryType.POINT;
      params.returnGeometry = false;

      //Consome serviço para pegar os municipios
      return Promise.all([
        //Todos as sedes municipais dentro do raio
        new GeoExecutor(this.request, this.response).exec('municipios', 'query', params, false),
        //Busca informações detalhadas do local
        new GeoExecutor(this.request, this.response).exec(params.service || 'bing', 'getLatLngDetails', params)
      ])
        .then(async ([municipio, details]) => {
          try {
            let munFind = (municipio.features && municipio.features.length > 0) ? municipio.features[0].properties : {
              municipio: '',
              uf: '',
              estado: '',
              regiao: '',
              geocodigo: '',
              regiao_metropolitana: ''
            }
            delete params.geometry;
            delete params.spatialRel
            delete params.geometryType
            params.where = "NOME like '%" + details.pais + "%'"
            let pais = await new GeoExecutor(this.request, this.response).exec('paises', 'query', params, false)
            let paisFind = (pais.features && pais.features.length > 0) ? pais.features[0].properties : {
              nome: '',
              codigo: ''
            }
            resolve({
              municipio: munFind.MUNICIPIO || munFind.municipio,
              estado: munFind.ESTADO || munFind.estado,
              uf: munFind.UF || munFind.uf,
              regiao: munFind.REGIAO || munFind.regiao,
              geocodigo: munFind.GEOCODIGO || munFind.geocodigo,
              regiao_metropolitana: munFind.REGIAO_METROPOLITANA || munFind.regiao_metropolitana || '',
              pais: details.pais || paisFind.NOME || paisFind.nome,
              codigo_un_pais: paisFind.CODIGO || paisFind.codigo,
              fullAddress: details.fullAddress || ''
            })
          } catch (error) {
            console.error(error);
            throw new Error(messages.M0008);
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  getDirections(params) {
    params.system = params.system || this.system
    return new Promise((resolve, reject) => {
      return new GeoExecutor(this.request, this.response)
        .exec(params.service, 'getDirections', params)
        .then((geoJSON) => {
          resolve(geoJSON);
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  exportRoute(params) {
    params.system = params.system || this.system
    switch (params.format) {
      case 'shp':
        return this.exportShapeFile(params)
      case 'kml':
        return this.exportKML(params)
      default:
        throw new Error()
    }
  }

  exportKML(params) {
    params.system = params.system || this.system
    return new Promise((resolve, reject) => {
      try {
        if (!params.geoJSON) {
          throw new Error(messages.M0010)
        }

        resolve(tokml(params.geoJSON))
      } catch (err) {
        reject(err)
      }
    })
  }

  exportShapeFile(params) {
    params.system = params.system || this.system
    return new Promise((resolve, reject) => {
      try {
        const types = {
          Point: 'POINT',
          LineString: 'POLYLINE',
          Polygon: 'POLYGON'
        }
        const promises = []

        params.geoJSON.features.forEach(f => {
          promises.push(new Promise((resolve, reject) => {
            const type = f.geometry.type
            shpWrite.write([this._fixExportShpefileProperties(f.properties)],
              types[type],
              [type === 'Point' ? f.geometry.coordinates : [f.geometry.coordinates]],
              (err, files) => {
                if (err) {
                  reject(err)
                } else {
                  resolve(files)
                }
              })
          }))
        })

        Promise.all(promises).then(files => {
          const filename = './shape-' + (new Date()).getTime()
          const zip = new Zip()
          files.forEach((filesShp, i) => {
            zip.file(`${filename}-${i}.shp`, filesShp.shp.buffer, {binary: true})
            zip.file(`${filename}-${i}.shx`, filesShp.shx.buffer, {binary: true})
            zip.file(`${filename}-${i}.dbf`, filesShp.dbf.buffer, {binary: true})
            zip.file(`${filename}-${i}.prj`, filesShp.prj)
          })
          const buf = zip.generate({type: 'nodebuffer', compression: 'STORE'})
          resolve(buf)
        })
      } catch (err) {
        reject(err)
      }
    })
  }

  importRoute(params) {
    params.system = params.system || this.system
    switch (params.format) {
      case 'shp':
        return this.importShapeFile(params)
      case 'kml':
        return this.importKML(params)
      default:
        throw new Error(messages.M0014)
    }
  }

  importKML(params) {
    params.system = params.system || this.system
    return new Promise((resolve, reject) => {
      try {
        if (!this.request.files.file) {
          throw new Error(messages.M0012)
        }
        let data = ''
        this.request.files.file.on('data', chunk => data += chunk)
        this.request.files.file.on('end', () => {
          let kml = new DOMParser().parseFromString(data, 'utf8')
          resolve(toGeoJSON.kml(kml))
        })
      } catch (error) {
        reject(error)
      }
    })
  }

  importShapeFile(params) {
    params.system = params.system || this.system
    return new Promise(async (resolve, reject) => {
      if (!this.request.files.file) {
        throw new Error(messages.M0013)
      }

      let data = Buffer.alloc(0)
      this.request.files.file.on('data', chunk => data = Buffer.concat([data, chunk]))
      this.request.files.file.on('end', async () => {
        try {
          const zip = new Zip(data, {base64: false})
          const files = {}
          let match = null
          let file = null

          for (const fileName in zip.files) {
            match = fileName.match(/\.(shp|dbf)/)
            if (match !== null) {
              file = fileName.replace(/\.(\\|\/)/, '').split(fileName.charAt(match.index))
              if (!files[file[0]]) {
                files[file[0]] = {dbf: null, shp: null}
              }
              files[file[0]][file[1]] = zip.files[fileName].asArrayBuffer()
            }
          }

          const shapes = Object.keys(files).map(v => files[v])

          if (shapes.filter(v => v.shp === null || v.dbf === null).length) {
            throw new Error(messages.M0013)
          }

          const geojsons = await Promise.all(shapes.map(shape => shapefile.read(shape.shp, shape.dbf)))
          resolve({
            type: 'FeatureCollection',
            features: geojsons.reduce((acc, val) => {
              return acc.concat(val.features.map(v => {
                v.properties = this._fixImportShapefileProperties(v.properties)
                return v
              }))
            }, [])
          })
        } catch (error) {
          throw new Error(messages.M0015)
        }
      })
    })
  }

  /*
   Retorna a distância entre ceps passando ao BING os 2 CEPS ou os endereços dos ceps retornados pelo webservice da ANTT
   */
  async getCEPDistance(params) {
    params.system = params.system || this.system
    params.cepFrom = params.cepFrom.replace('.','').replace('-','')
    params.cepTo = params.cepTo.replace('.','').replace('-','')
    /*try {
      if (config.webservice) {
        params.addressFrom = await this._getAddressFromCEPANTT(params.cepFrom, params.system)
        params.addressTo = await this._getAddressFromCEPANTT(params.cepTo, params.system)
      }
    }catch (e) {

    }*/
    let cache = await Cache.getCache('getCEPDistanceANTT', params)
    if (cache) {
      if (this.response) {
        this.response.setHeader('content-type', 'application/json');
      }
      return cache
    }
    return new Promise((resolve, reject) => {
      let paramsTo = {
        // address: params.addressTo || params.cepTo,
        address: params.cepTo,
        service: params.service,
        system: params.system
      }
      let paramsFrom = {
        //address: params.addressFrom || params.cepFrom,
        address: params.cepFrom,
        service: params.service,
        system: params.system
      }
      return Promise.all([
        new GeoExecutor(this.request, this.response).exec(params.service, 'getCoordinatesFromAddress', paramsFrom),
        new GeoExecutor(this.request, this.response).exec(params.service, 'getCoordinatesFromAddress', paramsTo),
      ])
        .then( async ([from, to]) => {
          let paramsMunicipioFrom = {}
          let paramsMunicipioTo = {}
          paramsMunicipioFrom.geometry = {};
          paramsMunicipioTo.geometry = {};
          if (from.error || from.length <=0 ) {
            paramsFrom.address = await this._getAddressFromCEPANTT(params.cepFrom, params.system)
            from = await new GeoExecutor(this.request, this.response).exec(params.service, 'getCoordinatesFromAddress', paramsFrom)
            if (from.error || from.length <=0 ) {
              throw new Error(messages.M0016)
            }
          }
          if (to.error || to.length <=0 ) {
            paramsTo.address = await this._getAddressFromCEPANTT(params.cepTo, params.system)
            to = await new GeoExecutor(this.request, this.response).exec(params.service, 'getCoordinatesFromAddress', paramsTo)
            if (to.error || to.length <=0 ) {
              throw new Error(messages.M0017)
            }
          }
          params.addressFrom = from[0].fullAddress
          params.addressTo = to[0].fullAddress
          paramsMunicipioFrom.where = '1=1';
          paramsMunicipioFrom.inSR = 4326;
          paramsMunicipioFrom.outFields = ['*'];
          paramsMunicipioFrom.geometry.lng = from[0].lng;
          paramsMunicipioFrom.geometry.lat = from[0].lat;
          paramsMunicipioFrom.spatialRel = SpatialRelEnum.Intersects;
          paramsMunicipioFrom.geometryType = GeometryType.POINT;
          paramsMunicipioFrom.returnGeometry = false;
          paramsMunicipioFrom.system = params.system

          //let paramsMunicipioTo = {}
          paramsMunicipioTo.where = '1=1';
          paramsMunicipioTo.inSR = 4326;
          paramsMunicipioTo.outFields = ['*'];
          //paramsMunicipioTo.geometry = {};
          paramsMunicipioTo.geometry.lng = to[0].lng;
          paramsMunicipioTo.geometry.lat = to[0].lat;
          paramsMunicipioTo.spatialRel = SpatialRelEnum.Intersects;
          paramsMunicipioTo.geometryType = GeometryType.POINT;
          paramsMunicipioTo.returnGeometry = false;
          paramsMunicipioTo.system = params.system

          let paramsRoute = {}
          paramsRoute.service = params.service
          paramsRoute.origin = {
            lat: from[0].lat,
            lng: from[0].lng
          }
          paramsRoute.destination = {
            lat: to[0].lat,
            lng: to[0].lng
          }
          paramsRoute.waypoints = []
          paramsRoute.mode = "DRIVING"
          paramsRoute.system = params.system

          //Consome serviço para pegar os municipios
          return Promise.all([
            //Todos as sedes municipais dentro do raio
            new GeoExecutor(this.request, this.response).exec('municipios', 'query', paramsMunicipioFrom),
            new GeoExecutor(this.request, this.response).exec('municipios', 'query', paramsMunicipioTo),
            new GeoExecutor(this.request, this.response).exec(paramsRoute.service, 'getDirections', paramsRoute)
          ])
            .then( async ([munFrom, munTo, route]) => {
              if (munFrom && munFrom.features.length <=0 ) {
                throw new Error(messages.M0018)
              }
              if (munFrom && munFrom.features.length <=0 ) {
                throw new Error(messages.M0019)
              }
              if (route && route.length <=0 ) {
                throw new Error(messages.M0019)
              }
              let serviceMetadata = Store.get('service:municipios')
              let result = {
                cepFrom: params.cepFrom,
                cepTo: params.cepTo,
                addressFrom: params.addressFrom || route[0].properties.start_address,
                addressTo: params.addressTo  || route[0].properties.end_address,
                distance:Number(route[0].properties.distance.value /1000).toFixed(2),
                system: params.system,
                service:'bing',//TODO: pegar o serviço ou cache
                dateTimeUpdate: moment().format("DD/MM/YYYY HH:mm:ss"),
                geocodigoFrom: munFrom.features[0].properties[serviceMetadata.nomeCampoGeoCodigo],
                geocodigoTo: munTo.features[0].properties[serviceMetadata.nomeCampoGeoCodigo]
              }
              Cache.putCache('getCEPDistanceANTT', params, result)
              resolve(result)
            })
            .catch(e => {
              reject(e);
            })
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  _getAddressFromCEPANTT(cep, sistema) {
    return new Promise((resolve, reject) => {
      let url = config.webservice + "endereco/" +cep
      let params = {
        siglaSistema: sistema
      }
      request({
          url,
          json: true,
          qs: params
        },
        (error, response) => {
          try {
            if (error) {
              throw error;
            }
            if (response.body && response.body.error) {
              throw response.body.error;
            }
            if (String(response.statusCode) !== '200') {
              if (response.body && response.body.Message) {
                throw new Error("Erro no consumo do do WebService '" + config.webservice + "' - Mensagem -> " + response.body.Message);
              }
              throw new Error('Erro no consumo do do WebService ' + config.webservice);
            }
            let addres = response.body.abreviacao.trim() + ' ' + response.body.bairro.trim() + ',' + response.body.municipio.trim() + ', ' + response.body.siglaUnidadeFederativa.trim()
            resolve(addres)
          } catch (error) {
            console.error('Erro no consumo de ' + config.webservice);
            reject(error);
          }
        });
    })

  }
  _fixExportShpefileProperties(geoJsonFeature) {
    const newFeat = {...geoJsonFeature}

    if (newFeat.steps) {
      newFeat.steps = JSON.stringify(newFeat.steps)
    }

    return newFeat
  }

  _fixImportShapefileProperties(properties) {
    const newProperties = {}
    const propertiesMap = {
      end_addr: 'end_address',
      routeTyp: 'routeType',
      start_ad: 'start_address'
    }

    Object.keys(properties).forEach(prop => {
      if (propertiesMap[prop]) {
        newProperties[propertiesMap[prop]] = properties[prop]
      } else {
        newProperties[prop] = properties[prop]
      }
    })

    if (newProperties.steps) {
      try {
        newProperties.steps = JSON.parse(newProperties.steps)
      } catch (e) {
      }
    }

    return newProperties
  }

  /*
  Retorna a distância entre 2 pontos passando ao BING
  */
  async getCoordinatesDistance(params) {
    params.system = params.system || this.system
    let cache = await Cache.getCache('getCoordinatesDistance', params)
    if (cache) {
      if (this.response) {
        this.response.setHeader('content-type', 'application/json');
      }
      return cache
    }
    return new Promise((resolve, reject) => {
      let paramsMunicipioFrom = {}
      let paramsMunicipioTo = {}
      paramsMunicipioFrom.geometry = {};
      paramsMunicipioTo.geometry = {};
      paramsMunicipioFrom.where = '1=1';
      paramsMunicipioFrom.inSR = 4326;
      paramsMunicipioFrom.outFields = ['*'];
      paramsMunicipioFrom.geometry.lng = params.pointFrom.lng;
      paramsMunicipioFrom.geometry.lat = params.pointFrom.lat;
      paramsMunicipioFrom.spatialRel = SpatialRelEnum.Intersects;
      paramsMunicipioFrom.geometryType = GeometryType.POINT;
      paramsMunicipioFrom.returnGeometry = false;
      paramsMunicipioFrom.system = params.system

      //let paramsMunicipioTo = {}
      paramsMunicipioTo.where = '1=1';
      paramsMunicipioTo.inSR = 4326;
      paramsMunicipioTo.outFields = ['*'];
      //paramsMunicipioTo.geometry = {};
      paramsMunicipioTo.geometry.lng = params.pointTo.lng;
      paramsMunicipioTo.geometry.lat = params.pointTo.lat;
      paramsMunicipioTo.spatialRel = SpatialRelEnum.Intersects;
      paramsMunicipioTo.geometryType = GeometryType.POINT;
      paramsMunicipioTo.returnGeometry = false;
      paramsMunicipioTo.system = params.system

      let paramsRoute = {}
      paramsRoute.service = params.service
      paramsRoute.origin = {
        lat: params.pointFrom.lat,
        lng: params.pointFrom.lng
      }
      paramsRoute.destination = {
        lat: params.pointTo.lat,
        lng: params.pointTo.lng
      }
      paramsRoute.waypoints = []
      paramsRoute.mode = "DRIVING"
      paramsRoute.system = params.system

      //Consome serviço para pegar os municipios
      return Promise.all([
        //Todos as sedes municipais dentro do raio
        new GeoExecutor(this.request, this.response).exec('municipios', 'query', paramsMunicipioFrom),
        new GeoExecutor(this.request, this.response).exec('municipios', 'query', paramsMunicipioTo),
        new GeoExecutor(this.request, this.response).exec(paramsRoute.service, 'getDirections', paramsRoute)
      ])
        .then( async ([munFrom, munTo, route]) => {
          if (munFrom && munFrom.features.length <=0 ) {
            throw new Error(messages.M0018)
          }
          if (munFrom && munFrom.features.length <=0 ) {
            throw new Error(messages.M0019)
          }
          if (route && route.length <=0 ) {
            throw new Error(messages.M0019)
          }
          let serviceMetadata = Store.get('service:municipios')
          let result = {
            pointFrom: params.pointFrom,
            pointTo: params.pointTo,
            addressFrom: route[0].properties.start_address,
            addressTo: route[0].properties.end_address,
            distance:Number(route[0].properties.distance.value /1000).toFixed(2),
            system: params.system,
            service:'bing',//TODO: pegar o serviço ou cache
            dateTimeUpdate: moment().format("DD/MM/YYYY HH:mm:ss"),
            geocodigoFrom: munFrom.features[0].properties[serviceMetadata.nomeCampoGeoCodigo],
            geocodigoTo: munTo.features[0].properties[serviceMetadata.nomeCampoGeoCodigo]
          }
          Cache.putCache('getCoordinatesDistance', params, result)
          resolve(result)
        })
        .catch(e => {
          reject(e);
        })
    })
  }
}

exports.GeoprocessorController = GeoprocessorController;
