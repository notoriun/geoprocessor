const {EndPoint} = require("../decorator/EndPoint");
const {HttpVerb} = require("../enum/HttpVerb");
const {GeometryType} = require("../enum/GeometryType");
const Store = require("../Store");
const {TypeRoutePoint} = require("../enum/TypeRoutePoint");
const {SpatialRelEnum} = require("../enum/SpatialRelEnum");
const config = require("../config");
const {messages} = require("../messages");
const GeoExecutor = require("../helper/GeoExecutor");
const Validator = require("../helper/Validator");
const Terraformer = require('terraformer');

class SLVController {

  constructor(request, response) {
    this.request = request;
    this.response = response;
    this.system = 'SLV';
  }

  getDirections(params) {
    return new Promise((resolve, reject) => {
      let estadosBrasil = Store.get('service:estados');
      let originUF;
      let destinationUF;
      let campoNomeUF = estadosBrasil.campoNomeUF;

      let defaulParams = {
        serviceId: estadosBrasil.id,
        outFields: [estadosBrasil.campoNomeUF],
        geometryType: GeometryType.POINT,
        spatialRel: SpatialRelEnum.Intersects,
        returnGeometry: false,
        inSR: 4326,
        outSR: 4326,
        system: this.system
      };

      if (params.waypoints) {
        params.waypoints = params.waypoints.sort(({type}) => {
          if (type === TypeRoutePoint.EMBARQUE) {
            return 0;
          } else {
            return 1;
          }
        });
      }
      params.system = this.system

      //Consome serviço para pegar as UFs que intersectam com os pontos de origem e destino final
      return Promise.all([
        new GeoExecutor(this.request, this.response).exec(estadosBrasil, 'query', Object.assign({}, defaulParams, {
          geometry: params.origin
        }), false),
        new GeoExecutor(this.request, this.response).exec(estadosBrasil, 'query', Object.assign({}, defaulParams, {
          geometry: params.destination
        }), false)
      ])
        .then((geoJSONs) => {
          //VALIDA: PONTO DE ORIGEM E DESTINO FINAL -> UFs DIFERENTES

          try {
            if (geoJSONs[0].features.length &&
              geoJSONs[0].features[0].properties &&
              geoJSONs[0].features[0].properties[campoNomeUF]
            ) {
              originUF = geoJSONs[0].features[0].properties[campoNomeUF];
            }

            if (geoJSONs[1].features.length &&
              geoJSONs[1].features[0].properties &&
              geoJSONs[1].features[0].properties[campoNomeUF]
            ) {
              destinationUF = geoJSONs[1].features[0].properties[campoNomeUF];
            }

            /*if (!originUF) {
              throw new Error(messages.M0001);
            }*/

            if (originUF === destinationUF) {
              throw new Error(messages.M0001);
            }

          } catch (error) {
            console.error(error);
            throw new Error(messages.M0001);
          }
        })
        .then(() => {
          //VALIDA: PONTOS DE EMBARQUE -> APENAS MESMA UF DO PONTO DE ORIGEM
          if (params.waypoints && params.waypoints.length) {
            let pontosPValidar = [];

            for (let waypoint of params.waypoints) {
              if (waypoint.type === TypeRoutePoint.EMBARQUE) {
                pontosPValidar.push(
                  this._validatePointsIntoUfOrigin([waypoint], estadosBrasil, campoNomeUF, originUF)
                );
              } else if (waypoint.type === TypeRoutePoint.DESEMBARQUE) {
                pontosPValidar.push(
                  this._validatePointsIntoUfOrigin([waypoint], estadosBrasil, campoNomeUF, destinationUF)
                );
              }
            }

            if (pontosPValidar.length) {
              return Promise.all(pontosPValidar)
                .then(result => {
                  for (let intoUfOrigin of result) {
                    if (intoUfOrigin === false) {
                      return reject(new Error(messages.M0003));
                    }
                  }

                });
            }
          }
        })
        .then(() => {
          //VALIDA: PONTOS DE DESTINO INTERMEDIÁRIO
          //Os pontos de destino (final e intermediário) só podem ser colocados em UFs diferentes da origem.
          if (params.waypoints && params.waypoints.length) {
            let pontosPValidar = [];

            for (let waypoint of params.waypoints) {
              if (waypoint.type === 'intermediario') {
                pontosPValidar.push(
                  this._validatePointsIntoUfOrigin([waypoint], estadosBrasil, campoNomeUF, originUF)
                );
              }
            }

            if (pontosPValidar.length) {
              return Promise.all(pontosPValidar)
                .then(result => {
                  for (let intoUfOrigin of result) {
                    if (intoUfOrigin === true) {
                      return reject(new Error(messages.M0006));
                    }
                  }

                });
            }
          }
        })
        .then(() => {
          //VALIDA: TODOS OS PONTOS FORA DO BRASIL -> Deve adicionar pontos de fronteira à rota

          let pontosDeFroteira = Store.get('pontosDeFronteira');
          let geometryBrasil = new Terraformer.Primitive({
            "type": 'MultiPolygon',
            coordinates: Store.get('limitesBrasil').geometry.coordinates
          });
          let pontosForaDoBrasil = [];
          let allPoints = [
            params.origin,
          ].concat(params.waypoints);
          allPoints.push(params.destination);

          //pegar os pontos fora do brasil
          for (let index in allPoints) {
            let point = allPoints[index];
            let instancePoint = new Terraformer.Primitive({
              "type": "Point",
              "coordinates": [point.lng, point.lat]
            });
            if (!instancePoint.within(geometryBrasil)) {
              pontosForaDoBrasil.push({
                point,
                index
              });
            }
          }

          if (pontosForaDoBrasil.length) {
            //consumo para buscar todas as direções entre cada ponto de fronteira e cada ponto fora do Brasil
            let promisesAll = [];

            for (let pontoFora of pontosForaDoBrasil) {
              let promisesGetDirections = [];

              for (let pontoFronteira of pontosDeFroteira) {
                promisesGetDirections.push(
                  new GeoExecutor(this.request, this.response).exec(params.service, 'getDirections', {
                    origin: {
                      lat: pontoFronteira.geometry.coordinates[1],
                      lng: pontoFronteira.geometry.coordinates[0]
                    },
                    destination: {
                      lat: pontoFora.point.lat,
                      lng: pontoFora.point.lng
                    },
                    inSR: params.outSR,
                    mode: params.mode,
                    outSR: params.outSR,
                    serviceId: params.service,
                    system: this.system
                  }, false)
                );
              }

              promisesAll.push(Promise.all(promisesGetDirections));
            }

            return Promise.all(promisesAll)
              .then(directionsEachOutPointBrasil => {
                // Tenta encontrar o ponto de fronteira mais próximo do ponto fora do Brasil

                for (let i in directionsEachOutPointBrasil) {
                  let routesEachPontoDeFronteira = directionsEachOutPointBrasil[i];
                  let menorDistancia;
                  let indexMenorRota;
                  let pontoFora = pontosForaDoBrasil[i];

                  for (let index in routesEachPontoDeFronteira) {
                    let routeGeoJSON = routesEachPontoDeFronteira[index];
                    let sumDistanceTrechos = 0;

                    if (routeGeoJSON.features && routeGeoJSON.features.length) {
                      for (let feature of routeGeoJSON.features) {
                        let distance = parseFloat(feature.properties['distance'].replace(/\./, '').replace(/,/, '.'));
                        sumDistanceTrechos += distance;
                      }

                      if (menorDistancia === void 0) {
                        menorDistancia = sumDistanceTrechos;
                        indexMenorRota = index;
                      }

                      if (sumDistanceTrechos < menorDistancia) {
                        menorDistancia = sumDistanceTrechos;
                        indexMenorRota = index;
                      }
                    }
                  }

                  if (indexMenorRota !== void 0) {
                    //Adiciona Ponto de Fronteira á Rota
                    let index_ = Number(pontoFora.index);

                    if (index_ === 0) {
                      index_ += 1;
                    }

                    allPoints.splice(index_, 0, {
                      type: 'fronteira',
                      indexMenorRota,
                      lat: pontosDeFroteira[indexMenorRota].geometry.coordinates[1],
                      lng: pontosDeFroteira[indexMenorRota].geometry.coordinates[0]
                    });
                  }
                }

                if (allPoints.length < 2) {
                  throw new Error(messages.M0005);
                }

                params.origin = allPoints[0];
                params.destination = allPoints[allPoints.length - 1];
                params.waypoints = [];

                for (let i in allPoints) {
                  let index = Number(i);
                  if (index > 0 && index < allPoints.length - 1) {
                    params.waypoints.push(allPoints[index]);
                  }
                }
              });
          }
        })
        .then(() => {
          //Após acabar todas as validações, enfim faz o consumo final
          return new GeoExecutor(this.request, this.response)
            .exec(params.service, 'getDirections', params)
            .then((geoJSON) => {
              resolve(geoJSON);
            });
        })
        .then(resolve)
        .catch(err => {
          console.log(err)
          reject(err)
        });
    });
  }

  _validatePointsIntoUfOrigin(pontosPValidar, serviceValidatorId, campoNomeUF, originUF) {
    return new GeoExecutor(this.request, this.response).exec(serviceValidatorId, 'query', {
      serviceId: serviceValidatorId.id,
      outFields: [campoNomeUF],
      geometryType: GeometryType.MULTI_POINT,
      spatialRel: SpatialRelEnum.Intersects,
      geometry: pontosPValidar,
      inSR: 4326
    }, false)
      .then((geoJSON) => {
        let ufPontosDeEmbarque = '';

        if (geoJSON.features.length && geoJSON.features[0].properties[campoNomeUF]) {
          ufPontosDeEmbarque = geoJSON.features[0].properties[campoNomeUF];
        }

        console.log(pontosPValidar, ufPontosDeEmbarque === originUF)
        return ufPontosDeEmbarque === originUF;
      });
  }

}

exports.SLVController = SLVController;
