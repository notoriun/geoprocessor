const {EndPoint, validateEntryParams}  = require("../../decorator/EndPoint");
const {HttpVerb}  = require("../../enum/HttpVerb");
const {messages}  = require("../../messages");
const GeoExecutor  = require("../../helper/GeoExecutor");

class WMSController {

    constructor(request, response) {
        this.request = request;
        this.response = response;
    }

    entryPoint(params) {
        let operationName = String(params.request).replace(/(.)/, (a, str) => str.toLowerCase());
        //validate
        params = this['_' + operationName + 'Params'](params);
        return new GeoExecutor(this.request, this.response).exec(params.serviceId, operationName, params, false);
    }

    _getMapParams(params) {
        let config = {
            version : {
                type : 'string',
                required : false,
                'default' : '1.1.1'
            },
            width : {
                type : 'number',
                required : true
            },
            height : {
                type : 'number',
                required : false
            },
            x : {
                type : 'number',
                required : false
            },
            y : {
                type : 'number',
                required : false
            },
            bbox : {
                type : 'string',
                required : true
            },
            srs : {
                type : 'string',
                required : true
            },
            transparent : {
                type : 'boolean',
                required : false,
                'default' : true
            },
            layers : {
                type : 'string',
                required : false,
                'default' : true
            },
            format : {
                type : 'string',
                required : false,
                'default' : 'png'
            }
        };

        return validateEntryParams(config, params);
    }

}

/*EndPoint({
    verb : HttpVerb.GET,
    route : '/v1/api/:serviceId/wms',
    response : {},
    params : {
        request : {
            type : 'string',
            required : true,
            validate : (value) => {
                let services = ['getmap'];
                if (services.indexOf(value.toLowerCase()) === -1) {
                    throw new Error(messages.M0001);
                }
                return value;
            }
        }
    }
})(WMSController, 'entryPoint');*/

module.exports = WMSController;