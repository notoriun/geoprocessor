const update = require('immutability-helper');
const xmldom = require('xmldom')
const {SpatialRelEnum} = require("../../../../enum/SpatialRelEnum");
const Store = require("../../../../Store");
const {GeometryType} = require("../../../../enum/GeometryType");
const ArcGISParser = require("terraformer-arcgis-parser");

const binaryOperators = {
  equalTo: 'PropertyIsEqualTo'
}

const GeoServerGeometryTypeMapping = {

  [GeometryType.POLYGON]: 'POLYGON',
  [GeometryType.LINE]: 'LINESTRING',
  [GeometryType.POINT]: 'POINT',
  [GeometryType.MULTI_POINT]: 'MULTIPOINT',
  [GeometryType.MULTI_LINE]: 'MULTILINESTRING'

};

const GeoServerSpatialRelMapping = {

  [SpatialRelEnum.Contains]: 'CONTAINS',
  [SpatialRelEnum.Crosses]: 'CROSSES',
  [SpatialRelEnum.EnvelopeIntersects]: 'INTERSECTS',
  [SpatialRelEnum.IndexIntersects]: 'INTERSECTS',
  [SpatialRelEnum.Intersects]: 'INTERSECTS',
  [SpatialRelEnum.Overlaps]: 'OVERLAPS',
  [SpatialRelEnum.Relation]: 'RELATE',
  [SpatialRelEnum.Touches]: 'TOUCHES',
  [SpatialRelEnum.Within]: 'WITHIN',

};

const GeoServerQueryParamsConvert = (serviceMetaData, appQueryParams) => {
  let attributesParams = {
    request: 'GetFeature',
    service: 'WFS',
    version: '2.0.0',
    outputFormat: 'application/json',
    typeNames: serviceMetaData.layerName
  }

  // attributesParams.count = '10'

  if (appQueryParams.inSR) {
    attributesParams.srsName = 'EPSG:' + appQueryParams.inSR
  }

  if (appQueryParams.outFields) {
    attributesParams.propertyName = appQueryParams.outFields.join(',')
    if (appQueryParams.returnGeometry) {
      if (serviceMetaData.nomeCampoGeometria) {
        attributesParams.propertyName += ',' + serviceMetaData.nomeCampoGeometria
      } else {
        throw new Error('Não foi definido o nome do campo de geometria nas configurações.')
      }
    }
  }

  let query = []

  if (appQueryParams.where) {
    query.push(appQueryParams.where)
  }

  if (serviceMetaData.nomeCampoGeometria &&
      appQueryParams.spatialRel &&
      appQueryParams.geometryType &&
      appQueryParams.geometry) {
    let geoCondition = [
      GeoServerSpatialRelMapping[appQueryParams.spatialRel], '(',
      serviceMetaData.nomeCampoGeometria, ', ',
      buildGeometrySql(appQueryParams.geometryType, appQueryParams.geometry), ')'
    ]
    query.push(geoCondition.join(''))
  }
  if (query.length > 0 && !(query.length === 1 && query[0] === 'string')) {
    attributesParams['cql_filter'] = query.join(' AND ')
  }

  return attributesParams
};

const exportParamsRequest = (appGetMapParams) => {

  let nParams = {};
  let serviceMetaData = Store.get('service:' + appGetMapParams['serviceId']);

  let mapping = {

    set bbox(v) {
      nParams['bbox'] = appGetMapParams['bbox'];
    },

    set size(v) {
      nParams['size'] = appGetMapParams.width + ',' + appGetMapParams.height;
    },

    set dpi(v) {
      nParams['dpi'] = 200;
    },

    set format(v) {
      nParams['format'] = appGetMapParams.format;
    },

    set layers(v) {
      nParams['layers'] = 'show:' + serviceMetaData.url.match(/(MapServer|FeatureServer)\/([\d]+)\/?/)[2];
    },

    set transparent(v) {
      nParams['transparent'] = appGetMapParams.transparent;
    },

    set f(v) {
      nParams['f'] = 'image';
    },

    set inSR(v) {
      nParams['inSR'] = appGetMapParams.inSR;
    }

  };

  let keys = Object.keys(mapping);

  for (let key of keys) {
    mapping[key] = appGetMapParams;
  }

  return nParams;
};

/**
 * @param {string} tagName
 * @param {string[]} attributes
 * @return {Document}
 */
function createXMLNode(tagName, attributes = [], value = '') {
  let str = ['<', tagName]

  for (let i = 0, ii = attributes.length; i < ii; i++) {
    if (attributes[i] instanceof Array) {
      str.push(' ')
      str.push(attributes[i][0])
      str.push('="')
      str.push(attributes[i][1])
      str.push('"')
    } else if (typeof attributes[i] === 'string') {
      str.push(' ')
      str.push(attributes[i][0])
    }
  }

  str.push('>')
  str.push(value)
  str.push('</')
  str.push(tagName)
  str.push('>')

  let parser = new xmldom.DOMParser()
  return parser.parseFromString(str.join(''), 'text/xml')
}

function buildGeometrySql (geometryType, geometry) {
  let ret = []

  if (GeoServerGeometryTypeMapping[geometryType] !== undefined && geometry) {
    ret.push(GeoServerGeometryTypeMapping[geometryType])
    ret.push('(')
    switch (geometryType) {
      case GeometryType.POLYGON:
        let coords = (geometry.type === GeometryType.MULTI_POLYGON) ? geometry.coordinates : [geometry.coordinates]
        let coordinatesPolygon = []
        for (let i = 0, ii = coords.length; i < ii; i++) {
          coordinatesPolygon.push(convertCoordinatesPolygon(coords[i]))
        }
        ret.push(coordinatesPolygon.join(', '))
        // ret.push(')')
        break
      case GeometryType.POINT:
        ret.push(convertCoordinatesPoint(geometry))
        break
      case GeometryType.MULTI_POINT:
        let multiPointcoords = []
        for (let i = 0, ii = geometry.length; i < ii; i++) {
          multiPointcoords.push('(' + convertCoordinatesPoint(geometry[i]) + ')')
        }
        ret.push(multiPointcoords.join(', '))
        break
    }
  }

  ret.push(')')

  return ret.join('')
}

function convertCoordinatesPoint (coordinatesPoint) {
  let coords = []
  coords.push(coordinatesPoint.lat)
  coords.push(' ')
  coords.push(coordinatesPoint.lng)
  return coords.join('')
}

function convertCoordinatesPolygon (coordinatesPolygon) {
  let coords = []
  let subCoords = null
  let paramsCoords = coordinatesPolygon
  let paramsSubCoords = null
  for (let i = 0, ii = paramsCoords.length; i < ii; i++) {
    paramsSubCoords = paramsCoords[i]
    subCoords = []
    for (let j = 0, jj = paramsSubCoords.length; j < jj; j++) {
      paramsSubCoordsLatLon = paramsSubCoords[j]
      subCoords.push([paramsSubCoordsLatLon[1], paramsSubCoordsLatLon[0]].join(' '))
    }
    coords.push('(' + subCoords.join(', ') + ')')
  }
  return coords.join(', ')
}

exports.GeoServerGeometryTypeMapping = GeoServerGeometryTypeMapping;
exports.GeoServerSpatialRelMapping = GeoServerSpatialRelMapping;
exports.GeoServerQueryParamsConvert = GeoServerQueryParamsConvert;
exports.exportParamsRequest = exportParamsRequest;
