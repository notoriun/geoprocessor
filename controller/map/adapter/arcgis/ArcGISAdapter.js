const request = require("request");
const {arcGISQueryParamsConvert, exportParamsRequest} = require("./Mapping");

class ArcGISAdapter {

  constructor(request, response) {
    this.request = request;
    this.response = response;
  }

  getMap(serviceMetaData, getMapParams) {
    return new Promise((resolve, reject) => {
      let params = exportParamsRequest(getMapParams);
      let url = serviceMetaData.url.replace(/(MapServer|FeatureServer).+/, '$1') + '/export';

      request({
        url,
        method: "GET",
        qs: params
      }, (error, response) => {
        try {
          if (error) {
            throw error;
          }

          if (response.statusCode !== 200) {
            throw new Error('Erro no consumo ArcGIS:getMap');
          }

        } catch (error) {
          reject(error);
        }
      }).pipe(this.response);
    });
  }

  query(serviceMetaData, queryParams) {
    return new Promise((resolve, reject) => {
      let url = serviceMetaData.url.replace(/\/$/, '') + '/query';
      let params = arcGISQueryParamsConvert(queryParams);

      request({
        url,
        method: "POST",
        form: params
      }, (error, response, data) => {
        try {
          if (error) {
            throw error;
          }

          data = JSON.parse(data);

          if (data.error) {
            throw data.error;
          }

          if (!data.features) {
            throw new Error('Erro no consumo ArcGIS:query');
          }

          resolve(data);

        } catch (error) {
          console.error('Erro no consumo ArcGIS:query');
          reject(error);
        }
      });
    });
  }

  getCoordinatesFromAddress(serviceMetaData, params) {
    return null;
  }

  getDirections(serviceMetaData, params) {
    return new Promise((resolve, reject) => {
      let url = serviceMetaData.url.replace(/\/$/, '') + '/solve';
      let features = [];
      let putFeature = ({lat, lng}) => {
        features.push({
          "geometry": {
            "x": lng,
            "y": lat,
            "spatialReference": {"wkid": params.inSR}
          }
        });
      };

      putFeature(params.origin);

      for (let point of params.waypoints) {
        putFeature(point);
      }

      putFeature(params.destination);

      let body = {
        // "barriers": "",
        // "polylineBarriers": "",
        // "polygonBarriers": "",
        // "outSR": "",
        // "travelMode": "",
        // "ignoreInvalidLocations": true,
        "accumulateAttributeNames": "Length%2C+Minutes",
        "impedanceAttributeName": "Length",
        "restrictionAttributeNames": "Oneway%2C+Restriction",
        // "attributeParameterValues": "",
        "restrictUTurns": "esriNFSBAllowBacktrack",
        "useHierarchy": true,
        "returnDirections": false,
        "returnRoutes": true,
        "returnStops": false,
        "returnBarriers": false,
        "returnPolylineBarriers": false,
        "returnPolygonBarriers": false,
        "directionsLanguage": "pt-BR",
        "directionsStyleName": "",
        "outputLines": "esriNAOutputLineTrueShape",
        "findBestSequence": true,
        "preserveFirstStop": true,
        "preserveLastStop": true,
        "useTimeWindows": false,
        "startTime": "0",
        "startTimeIsUTC": false,
        // "outputGeometryPrecision": "",
        "outputGeometryPrecisionUnits": "esriDecimalDegrees",
        "directionsOutputType": "esriDOTComplete",
        "directionsTimeAttributeName": "Minutes",
        "directionsLengthUnits": "esriNAUMeters",
        "returnZ": false,
        "f": 'geojson',
        "stops": JSON.stringify({features})
      };

      request({
          url,
          method: "GET",
          qs: body
        },
        (error, response, data) => {
          try {
            if (error) {
              throw error;
            }

            if (response.statusCode !== 200) {
              throw (typeof data === 'string' ? new Error(data) : data);
            }

            data = JSON.parse(data);

            if (data.error) {
              throw data.error;
            }

            if (!data.routes) {
              throw new Error('Erro no consumo ArcGIS:getDirections');
            }

            let geJSON = data.routes;

            for (let i in data.routes.features) {
              let feature = data.routes.features[i];
              let attributes = feature.attributes;
              geJSON.features[i].properties = {
                distance: attributes.Total_Length,
                duration: attributes.Total_Minutes,
                start_address: attributes.Name,
                end_address: attributes.Name
              };
            }

            resolve(geJSON);

          } catch (error) {
            reject(error);
          }
        });
    });
  }
}

module.exports.ArcGISAdapter = ArcGISAdapter;
