const {GeometryType} = require('../../../../enum/GeometryType');
const request = require("request");
const {SpatialRelEnum} = require("../../../../enum/SpatialRelEnum");
const GeoExecutor = require("../../../../helper/GeoExecutor");

const bingURL = "https://atlas.microsoft.com";

class BingMapsAdapter {

  constructor () {
    this.serviceMetaData;
  }


  getMap (serviceMetaData, getMapParams) {
    return null;
  }

  query (serviceMetaData, queryParams) {
    return new Promise((resolve, reject) => {
      resolve(null)
    });
  }

  getCoordinatesFromAddress (serviceMetaData, params) {
    return new Promise((resolve, reject) => {
      let url = bingURL + '/search/fuzzy/json';
      let bingParams = {
        typeahead: true,
        "subscription-key": serviceMetaData.key,
        "api-version": 1,
        maxFuzzyLevel: 3,
        query: params.address,
        language: 'pt-BR',
        countrySet: 'BR'
      };

      request({
          url,
          json: true,
          qs: bingParams
        },
        (error, response, geoCodeResponse) => {
          try {
            if (error) {
              throw error;
            }
            if (response.body.error) {
              throw response.body.error;
            }
            if (geoCodeResponse && geoCodeResponse.results.length === 0) {
              resolve({
                error: 'Não foram encontrados resultados'
              });
            }

            if (String(response.statusCode) !== '200') {
              resolve({
                error: 'Erro no consumo do Bing Maps. Verifique as chaves do BING.'
              });
            }

            let {results} = geoCodeResponse;

            if (!results.length) {
              console.log('geoCodeResponse', geoCodeResponse)
              resolve({
                error: 'Não foram encontrados resultados'
              });
            }

            let placeDetails = results.filter(result => {
              return result.entityType !== 'Country'
            }).map((result) => {
              return {
                lat: result.position.lat,
                lng: result.position.lon,
                fullAddress: result.address.freeformAddress + String(result.address.countrySubdivision ? ', ' + this._getUFFromEstado(result.address.countrySubdivision):''),
                local: result.address.streetName,
                municipio: result.address.municipality.split(',')[0],
                uf: this._getUFFromEstado(result.address.countrySubdivision),
                pais: result.address.country
              }
            });
            resolve(placeDetails)

          } catch (error) {
            reject(error);
          }

        });
    });
  }

  getLatLngDetails(serviceMetaData, params) {
    return new Promise((resolve, reject) => {
      let url = bingURL + '/search/address/reverse/json';
      let bingParams = {
        typeahead: true,
        "subscription-key": serviceMetaData.key,
        "api-version": 1,
        maxFuzzyLevel: 3,
        query: params.lat +  ',' + params.lon,
        language: 'pt-BR',
        countrySet: 'BR'
      };
      request({
        url,
        json: true,
        qs: bingParams
      }, (error, response, body) => {
        try {
          if (error) {
            throw new Error('Erro ao buscar detalhes do local (google places details)');
          } else if (body.summary.numResults <= 0 || !body.addresses[0].address.freeformAddress) {
            throw  new Error('Não foram encontrado resultados');
          } else if (response.statusMessage === "OK") {
            let result = {}
            if (body.addresses.length && body.addresses.length >0) {
              let pos = body.addresses[0].position.split(',')
              result = {
                lat: pos[0],
                lng: pos[1],
                fullAddress: body.addresses[0].address.freeformAddress + ', ' + this._getUFFromEstado(body.addresses[0].address.countrySubdivision),
                local: body.addresses[0].address.streetName,
                municipio: body.addresses[0].address.municipality,
                uf: this._getUFFromEstado(body.addresses[0].address.countrySubdivision),
                pais: body.addresses[0].address.country
              }
            }

            resolve(result)
          }
        } catch (error){
          reject(error);
        }
      });
    })
  }

  getDirections (serviceMetaData, params) {
    this.serviceMetaData = serviceMetaData
    return new Promise((resolve, reject) => {
      let url = bingURL + '/route/directions/json';
      let bingParams = {
        "subscription-key": serviceMetaData.key,
        "api-version": 1,
        "travelMode": 'bus',
        "routeType": 'shortest',
        "language": 'pt-BR',
        "instructionsType": 'text',
        "computeBestOrder": true,
        "routeRepresentation": 'polyline'
      };
      if (params.mode.toUpperCase() === 'BUS' || params.mode.toUpperCase() === 'TRANSIT') {
        bingParams.travelMode = 'bus';
      }

      bingParams.query = params.origin.lat + ',' + params.origin.lng + ':'
      for (let point of (params.waypoints || [])) {
        bingParams.query += point.lat + ',' + point.lng + ':';
      }
      bingParams.query += params.destination.lat + ',' + params.destination.lng

      request({
          url,
          method: "GET",
          qs: bingParams
        },
        async (error, response, routeResponse) => {
          if (error) {
            reject(error);
          } else if (String(response.statusCode) !== '200') {
            reject(new Error('Erro no consumo do Bing Maps'));
          } else {
            routeResponse = JSON.parse(response.body);

            console.log('BingMaps:getDirections#status', response.statusCode);

            let features = [];
            let indexInstructions = 0
            let lastLocationWayPointIndex = -1
            for (let leg of routeResponse.routes[0].legs) {
              let feature = {
                type: "Feature",
                properties: {
                  distance: {
                    text: this._formatDistance(leg.summary.lengthInMeters),
                    value: leg.summary.lengthInMeters
                  },
                  duration: {
                    text: this._formatDuration(leg.summary.travelTimeInSeconds),
                    value: leg.summary.travelTimeInSeconds
                  }
                },
                geometry: {
                  type: GeometryType.LINE,
                  coordinates: leg.points.map(p => {
                    return [p.longitude, p.latitude]
                  })
                },
                steps: []
              }
              while (indexInstructions < routeResponse.routes[0].guidance.instructions.length) {
                let instruction = routeResponse.routes[0].guidance.instructions[indexInstructions]
                if (instruction.instructionType !== 'LOCATION_WAYPOINT' || lastLocationWayPointIndex === indexInstructions ) {
                  if (instruction.instructionType === 'LOCATION_DEPARTURE' || instruction.instructionType === 'LOCATION_WAYPOINT') {
                    let params = {
                      lat: instruction.point.latitude,
                      lon: instruction.point.longitude
                    }
                    let add = await  this.getLatLngDetails(this.serviceMetaData, params)
                    feature.properties.start_address = add.fullAddress // instruction.street
                  }
                  if (instruction.instructionType === 'LOCATION_ARRIVAL') {
                    let params = {
                      lat: instruction.point.latitude,
                      lon: instruction.point.longitude
                    }
                    let add = await  this.getLatLngDetails(this.serviceMetaData, params)
                    feature.properties.end_address = add.fullAddress //instruction.street
                  }

                  let step = {}
                  let nextInstruction = (indexInstructions + 1) >= routeResponse.routes[0].guidance.instructions.length ? routeResponse.routes[0].guidance.instructions[indexInstructions] : routeResponse.routes[0].guidance.instructions[indexInstructions + 1]
                  let distance = nextInstruction.routeOffsetInMeters - instruction.routeOffsetInMeters
                  let time = nextInstruction.travelTimeInSeconds - instruction.travelTimeInSeconds
                  let coordinates = []
                  if (indexInstructions === 0) {
                    coordinates.push([instruction.point.longitude, instruction.point.latitude])
                  }
                  step.distance = {
                    text: this._formatDistance(distance),
                    value: distance
                  }
                  step.duration = {
                    text: this._formatDuration(time),
                    value: time
                  }
                  for (let point of leg.points) {
                    coordinates.push([point.longitude, point.latitude])
                    if (point.latitude == nextInstruction.point.latitude && point.longitude == nextInstruction.point.longitude) {
                      break
                    }
                  }
                  step.end_location = {
                    lat: coordinates[coordinates.length - 1][0],
                    lng: coordinates[coordinates.length - 1][1]
                  }
                  step.start_location = {
                    lat: coordinates[0][0],
                    lng: coordinates[0][1]
                  }
                  step.html_instructions = instruction.message
                  step.travel_mode = 'DRIVING'
                  step.geometry = {
                    type: GeometryType.LINE,
                    coordinates: coordinates
                  }
                  feature.steps.push(step)
                  indexInstructions++
                } else {
                  feature.properties.end_address = instruction.street
                  lastLocationWayPointIndex = indexInstructions
                  break
                }

              }
              features.push(feature);
            }
            resolve(features);
          }
        });
    });
  }

  _formatDistance(m) {
    return ((m/1000) + ' km').replace('.',',')
  }

  _formatDuration(s) {
    let f = s/60
    if (f < 1) {
      return '< 1 min'
    } else {
      return parseInt(f) + ' minutos'
    }
  }

  _getUFFromEstado (estado) {
    if (!estado) {
      return ''
    }
    estado = estado.toUpperCase()
    let uf = {
      "ACRE": "AC",
      "ALAGOAS": "AL",
      "AMAZONAS": "AM",
      "AMAPÁ": "AP",
      "AMAPA": "AP",
      "BAHIA": "BA",
      "CEARÁ": "CE",
      "CEARA": "CE",
      "DISTRITO FEDERAL": "DF",
      "ESPÍRITO SANTO": "ES",
      "ESPIRITO SANTO": "ES",
      "GOIÁS": "GO",
      "GOIAS": "GO",
      "MARANHÃO": "MA",
      "MARANHAO": "MA",
      "MINAS GERAIS": "MG",
      "MATO GROSSO DO SUL": "MS",
      "MATO GROSSO": "MT",
      "PARÁ": "PA",
      "PARA": "PA",
      "PARAÍBA": "PB",
      "PARAIBA": "PB",
      "PERNAMBUCO": "PE",
      "PIAUÍ": "PI",
      "PIAUI": "PI",
      "PARANÁ": "PR",
      "PARANA": "PR",
      "RIO DE JANEIRO": "RJ",
      "RIO GRANDE DO NORTE": "RN",
      "RONDÔNIA": "RO",
      "RONDONIA": "RO",
      "RORAIMA": "RR",
      "RIO GRANDE DO SUL": "RS",
      "SANTA CATARINA": "SC",
      "SERGIPE": "SE",
      "SÃO PAULO": "SP",
      "SAO PAULO": "SP",
      "TOCANTÍNS": "TO",
      "TOCANTINS": "TO"
    }
    if (uf[estado]) {
      return uf[estado]
    } else {
      return ''
    }

  }
}

module.exports.BingMapsAdapter = BingMapsAdapter;
