const {GeometryType} = require("../enum/GeometryType");
const Store = require("../Store");
const {SpatialRelEnum} = require("../enum/SpatialRelEnum");
const {messages} = require("../messages");
const GeoExecutor = require("../helper/GeoExecutor");
const Terraformer = require('terraformer')

class SGAController {

  constructor(request, response) {
    this.request = request;
    this.response = response;
    this.system = 'SGA'
  }

  /*
  Retorna os muncípios do raio de influência utilizando as regras do SGA RN07 e RN016
   */
  getCitiesWithinInfluenceRadius(params) {
    return new Promise((resolve, reject) => {
      let sedesMunicipais = Store.get('service:sedesMunicipais');
      let ride = Store.get('service:ride');
      let result =[];

      //gerar buffer
      let buffered = (new Terraformer.Circle([params.lon, params.lat], params.radius * 1000)).geometry;
      let promises = [];

      //Consome serviço para pegar os municipios
      return Promise.all([
        //Todos as sedes municipais dentro do raio
        new GeoExecutor(this.request, this.response).exec(sedesMunicipais, 'query', {
          outFields: [sedesMunicipais.campoCodigoIBGE],
          geometryType: GeometryType.POLYGON,
          spatialRel: SpatialRelEnum.Intersects,
          returnGeometry: false,
          where: '1=1',
          inSR: 4326,
          geometry: buffered,
          system: this.system
        }, false),
        //Busca se tem ride ou rm
        new GeoExecutor(this.request, this.response).exec(ride, 'query', {
          outFields: ['*'],
          geometryType: GeometryType.POINT,
          spatialRel: SpatialRelEnum.Intersects,
          returnGeometry: true,
          where: '1=1',
          inSR: 4326,
          geometry: {
            lng: params.lon,
            lat: params.lat
          },
          system: this.system
        }, false)
      ])
        .then(([sedes, ride]) => {
          try {
            result = sedes.features.map((sede)=>{
              return sede.properties[sedesMunicipais.nomeCampoCodigo]
            })
            if (ride.features && ride.features.length >0 ){
              return new GeoExecutor(this.request, this.response).exec(sedesMunicipais, 'query', {
                outFields: [sedesMunicipais.campoCodigoIBGE],
                geometryType: GeometryType.POLYGON,
                spatialRel: SpatialRelEnum.Intersects,
                returnGeometry: false,
                where: '1=1',
                inSR: 4326,
                geometry: ride.features[0].geometry,
                system: this.system
              }, false)
            }


          } catch (error) {
            console.error(error);
            throw new Error(messages.M0008);
          }
        })
        .then((munRide)=>{
          try {
            if (munRide && munRide.features) {
              let muninicipios = munRide.features.map((mun)=>{
                return mun.properties[sedesMunicipais.nomeCampoCodigo]
              })
              //Se o tipoVerificacao == analise, só considera os munipios da ride e descarta do raio.
              if (params.type === 'analise') {
                result = [];
              }
              result = result.concat(muninicipios);
            }

            let unique = result.filter( function( elem, i, array ) {
              return array.indexOf( elem ) === i;
            } );
            resolve(unique);

          } catch (error) {
            console.error(error);
            throw new Error(messages.M0008);
          }

        })
        .catch(reject);
    });
  }


}

exports.SGAController = SGAController;
