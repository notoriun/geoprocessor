const {EndPoint, getConfigurations} = require("../../decorator/EndPoint");
const {HttpVerb}  = require("../../enum/HttpVerb");

class APIDocsController {

    getAPIDescriptor() {
        return new Promise((resolve, reject) => {
            try {
                let definitions = {};
                let configurations = getConfigurations();
                let paths = {};

                for (let route in configurations) {
                    paths[route] = {};

                    for (let verb in configurations[route]) {
                        let {params} = configurations[route][verb];
                        let {tags} = configurations[route][verb];
                        let {description} = configurations[route][verb];
                        let {summary} = configurations[route][verb];
                        let response = configurations[route][verb]['response'] || {
                          "200": {
                            "description": "",
                            "schema": {}
                          }
                        }
                        params = params || {};
                        let schema = {};
                        let parameters = [{
                          schema: {
                            properties: {}
                          }
                        }];
                        for (let key in params) {
                            if (params[key].hasOwnProperty('in') && params[key].in === 'body'){
                              parameters[0].in = 'body';
                              parameters[0].name ='body';
                              break;
                            }
                        }
                        parameters[0].schema.properties = params;
                        if (!parameters[0].hasOwnProperty('in')) {
                         parameters[0].in = 'body';
                         parameters[0].name = 'body';
                        }
                        paths[route][verb] = {
                            "tags": tags,
                            "description": description,
                            "summary": summary,
                            "produces": [
                                "application/json"
                            ],
                            parameters,
                            "responses": response
                        };
                    }
                }
                delete paths['/v1/api/api-docs'];
                resolve({
                    "swagger": "2.0",
                    "info": {
                        "title": "Barramento de Serviços Geográficos",
                        "description": "**Este é um barramento de serviços geográficos. Aqui estão disponíveis diversas funções geográficas úteis para serem utilizadas por aplicativos.**",
                        "version": "1.0.0"
                    },
                    "responses": {},
                    "parameters": {},
                    "securityDefinitions": {},
                    paths,
                    definitions
                });
            } catch (error) {
                reject(error);
            }
        });
    }

}

exports.APIDocsController = APIDocsController;