/**
 * Verbos do HTTP
 * @enum
 */
exports.HttpVerb = {
    GET : 'get',
    POST : 'post',
    OPTIONS : 'options'
};