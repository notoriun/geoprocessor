let data = {};

const Store = {

    get(key) {
        return data[key];
    },

    set(key, value) {
        data[key] = value;
    },

    getAll() {
        return data;
    }

};

module.exports = Store;