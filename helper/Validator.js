const Validator = {

    verifyNestedValue(obj, path) {
        let parts = path.split('.');

        let verify = (currentObject, index) => {
            if ((parts.length - 1) === index) {
                return Boolean(parts[index] in currentObject);
            } else if (parts[index] in currentObject && typeof currentObject[parts[index]] === 'object') {
                return verify(currentObject[parts[index]], index + 1);
            } else {
                return false;
            }
        };

        return verify(obj, 0);
    }

};

module.exports = Validator;