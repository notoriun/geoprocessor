const express = require("express");
const bodyParser = require('body-parser');
const queryParser = require('query-parser');
const Sender = require("./midlleware/Sender");
const Getter = require("./midlleware/Getter");
const config = require("./config");
const {loadEndPoints} = require("./decorator/EndPoint");
const fs = require("fs");
const path = require("path");
const Store = require("./Store");
const helmet = require('helmet');
const compression = require('compression');
const spdy = require('https');
const constants = require('constants');
const swaggerJSDoc = require('swagger-jsdoc');
const formData = require('express-form-data')
const os = require('os')
const pathRoute = __dirname + '/routes/**/*.js';
const initDatabase = require('./db')
const options = {
  swaggerDefinition: {
    info: {
      title: 'Barramento de Serviços Geográficos',
      description: '**Este é um barramento de serviços geográficos. Aqui estão disponíveis diversas funções geográficas úteis para serem utilizadas por aplicativos.**',
      version: '1.0.0',
    }
  },
  apis: [pathRoute]
};
const swaggerSpec = swaggerJSDoc(options);
let HTTP_PORT = (process.env.NODE_ENV === 'production') ? (process.env.HTTP_PORT || config.protocols.http.port) : 8081;

for (let serviceData of config.services) {
  Store.set('service:' + serviceData.id, serviceData);
}

class Server {

  constructor() {
    const etl = require("./etl");
    this.dbConnection =
    this._server = express();
    let pathData = path.resolve(serverPath, 'data');
    if (!fs.existsSync(pathData)) {
      fs.mkdirSync(pathData);
    }
    this._server.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
      res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

      if ('OPTIONS' === req.method) {
        res.sendStatus(200);
      } else {
        next();
      }
    });
    this._registryMiddlewares();
    this._server.use(express.static(path.resolve(__dirname, 'client')));
    this._server.use('/api-doc', express.static(path.resolve(__dirname, 'api-doc')));
    this._server.get('/api-docs.json', function(req, res) {
      res.setHeader('Content-Type', 'application/json');
      res.send(swaggerSpec);
    });
    initDatabase().then(dbConnection => {
      this.dbConnection = dbConnection
      this._server.listen(HTTP_PORT, config.host, () => {
        loadEndPoints()
          .then(() => etl.load())
          .then(() => {
            console.log(`Server HTTP pronto rodando na porta: ${HTTP_PORT}`);
            if (config.protocols.hasOwnProperty('https') && config.protocols.https.hasOwnProperty('port')) {
              spdy.createServer({
                key: fs.readFileSync(path.resolve(config.protocols.https.certificates.key)),
                cert: fs.readFileSync(path.resolve(config.protocols.https.certificates.crt)),
                secureOptions: constants.SSL_OP_NO_TLSv1 | constants.SSL_OP_NO_TLSv1_1
              }, this._server).listen(process.env.HTTPS_PORT || config.protocols.https.port, () => {
                console.log(`Server HTTPS pronto rodando na porta: ${process.env.HTTPS_PORT || config.protocols.https.port}`);
              });
            }
            this._server.emit('appStarted');
          })
      });
    })
  }

  _registryMiddlewares() {
    // this._server.use(queryParser);
    this._server.use(helmet.hsts({
      maxAge: 31536000,
      includeSubDomains: true,
      preload: true
    }));
    const options = {
      uploadDir: os.tmpdir(),
      autoClean: true
     };

     // parse data with connect-multiparty.
     this._server.use(formData.parse(options));
     // clear from the request and delete all empty files (size == 0)
     this._server.use(formData.format());
     // change file objects to stream.Readable
     this._server.use(formData.stream());

    this._server.use(helmet.noCache());
    this._server.use(helmet.noSniff());
    this._server.use(bodyParser.json({limit: '50mb'}));
    this._server.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
    this._server.use(compression({threshold:0,filter: this._shouldCompress}));
    this._server.use(this._registryMidCorsOrigin.bind(this));
    this._server.use(this._registryMidSender.bind(this));
    this._server.use(this._registryMidGetter.bind(this));
    if (process.env.NODE_ENV === 'production') {
        this._server.use(this._redirectHTTPS.bind(this));
    }
  }

  _registryMidCorsOrigin(req, res, next) {
    res.header("Access-Control-Allow-Origin", req.header("Origin"));
    res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
    res.header("Access-Control-Max-Age", "3600");
    res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
    res.header("Access-Control-Allow-Credentials", "true");
    next();
  };

  _registryMidSender(req, res, next) {
    res['sender'] = new Sender(req, res);
    next();
  };

  _registryMidGetter(req, res, next) {
    req.getter = new Getter(req);
    next();
  };

  _redirectHTTPS (req,res,next){
    let host,url;
    host = req.headers['host'];
    if (host){
      host = host.split(':');
      host = host[0];
      url = (req.url)?req.url:'/';
      if ((!req.secure && config.protocols.hasOwnProperty("https") && config.protocols.https.hasOwnProperty("certificates") && fs.existsSync(path.resolve(config.protocols.https.certificates.crt)) && fs.existsSync(path.resolve(config.protocols.https.certificates.key)))) {
        res.redirect('https://' + host + ':' + (process.env.HTTPS_PORT || config.protocols.https.port) + url);
        return;
      }
    }
    next();
  }

  _shouldCompress(req, res) {
    if (req.headers['x-no-compression']) {
      return false;
    }

    return compression.filter(req, res);
  }

  putRoute(verb, path, ...callback) {
    let args = [path];
    for (let cb of callback) {
      args.push(cb);
    }
    this._server[verb].apply(this._server, args, this.dbConnection);
  }

}

exports.Server = Server;
