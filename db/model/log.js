const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const LogSchema = new Schema({
  sistema: {
    type: String,
    required: [true, "Sistema que executou a operação"]
  },
  url: {
    type: String,
    required: [true, "Url que disparou o método"]
  },
  fonte: {
    type: String,
    required: [true, "Fonte da informação"]
  },
  metodo: {
    type: String,
    required: [true, "Método que realizou a execução"]
  },
  dataCriacao: {
    type: Date,
    default: Date.now
  },
  timeZoneOffset: {
    type: Number,
    default: (new Date().getTimezoneOffset())
  }
});
const Log = mongoose.model("Log", LogSchema);
module.exports = Log;
