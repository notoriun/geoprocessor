module.exports = class Getter{

    constructor(request) {
        this.request = request;
    }

    getParams() {
        let paramsRequest = this.request.body ? this.request.body : {};
        paramsRequest = Object.assign({}, paramsRequest, (this.request.params || {}));
        paramsRequest = Object.assign({}, paramsRequest, (this.request.query || {}));
        return paramsRequest;
    }

    getParam(paramName) {
        let param;

        if (this.request.params && paramName in this.request.params) {
            param = this.request.params[paramName];

        } else if (this.request.query && paramName in this.request.query) {
            param = this.request.query[paramName];

        } else if (this.request.params && paramName in this.request.params) {
            param = this.request.params[paramName];

        } else if (this.request.body && paramName in this.request.body) {
            param = this.request.body[paramName];

        }
        return param;
    }

};
