const {EndPoint} = require("../../../decorator/EndPoint");
const {HttpVerb} = require("../../../enum/HttpVerb");
const {SIRController} = require('../../../controller/sirController/SIRController');

/**
 * /v1/api/SGA/get
 */
EndPoint({
  verb: [HttpVerb.POST],
  tags: ['SIR'],
  route: '/v1/api/SIR/categorizePointsOfRodovias',
  summary: 'Categoriza pontos em um raio limite de rodovias.',
  description: 'Recebe um arquivo CSV com pontos e retorna os pontos categorizados se estão dentro ou fora de um raio limite de rodovias.',
  response: {
    '200': {
      description: 'Os pontos do CSV categorizados em dentro e fora da distância limite.',
      schema: {
        type: 'object'
      }
    }
  },
  params: {
    file: {
      type:  'string',
      format: 'binary',
      required: true,
      description: 'O arquivo CSV contendo os pontos a serem categorizados.'
    },
    distanceToRodovias: {
      type: 'number',
      required: false,
      default: 5,
      example: 5,
      description: 'A distância limite para categorizar os pontos do CSV.'
    },
    latCol: {
      type : 'string',
      required: false,
      default: 'Latitude',
      example: 'Latitude',
      description: 'A coluna de latitude do arquivo CSV.'
    },
    lonCol: {
      type: 'string',
      required: false,
      default: 'Longitude',
      example: 'Longitude',
      description: 'A coluna de longitude do arquivo CSV.'
    }
  }
})(SIRController, 'categorizePointsOfRodovias');
