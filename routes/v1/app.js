const {EndPoint}  = require("../../decorator/EndPoint");
const {HttpVerb}  = require("../../enum/HttpVerb");
const {APIDocsController} = require("../../controller/app/APIController")

EndPoint({
  verb : [HttpVerb.GET],
  route : '/v1/api/api-docs',
  response : {}
})(APIDocsController, 'getAPIDescriptor');