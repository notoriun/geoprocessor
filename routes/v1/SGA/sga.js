const {EndPoint} = require("../../../decorator/EndPoint");
const {HttpVerb} = require("../../../enum/HttpVerb");
const {SpatialRelEnum} = require("../../../enum/SpatialRelEnum");
const {SGAController} = require('../../../controller/SGAController');
const {TypeRoutePoint}  = require("../../../enum/TypeRoutePoint");

/**
 * /v1/api/SGA/get
 */
EndPoint({
  verb: [HttpVerb.POST],
  tags: ['SGA'],
  route: '/v1/api/SGA/getCitiesWithinInfluenceRadius',
  summary: 'Retorna todos os municipíos de um RAIO de influência ou que façam parte de uma Região Metropolitana ou RIDE. ',
  description: 'Verifica a influência do mercado de acordo com as regras de adição ou análise de mercado. ',
  response: {
    '200': {
      description: 'Array contendo o código IBGE de todos municípios do raio informatou  ou RM/RIDE ',
      schema: {
        type: 'array',
        example: [5200175,5200803,5203962]
      },

    },
    '400': {
      description: 'Operação inválida',
    },
  },
  params: {
    type:{
      type:  'string',
      enum: ['adicionar', 'analise'],
      required: true,
      example: 'adicionar',
      description: 'Determina o comportamento da verificação do raio de influência conforme as regras de \'Análise de solicitação de mercado\' e \'Verificação ao adicionar mercado\' . \n' +
      'Caso \'adicionar\':\n' +
      'Se o ponto pertence à uma RM ou RIDE, são considerados todos os municípios da Região Metropolitana ou RIDE, mais os municípios com sede dentro do círculo do raio informado, mesmo que esteja fora da região metropolitana. Se o ponto não pertence à uma RM/RIDE, são considerados todos os municípios em que a sede está completamente dentro do raio informado.\n' +
      'Caso \'analise\':\n' +
        'Se o ponto pertence à uma RM ou RIDE, são considerados todos os municípios da Região Metropolitana ou RIDE.  Se o ponto NÃO pertence à uma RM ou RIDE são considerados os municípios com sede dentro do círculo do raio informado. '
    },
    radius: {
      type: 'number',
      required: true,
      example: 50,
      description: 'Raio (em Km) a ser utilizado na busca'
    },
    lat : {
      type : 'number',
      required : true,
      example: -15
    },
    lon : {
      type : 'number',
      required : true,
      example: -47
    }
  }
})(SGAController, 'getCitiesWithinInfluenceRadius');